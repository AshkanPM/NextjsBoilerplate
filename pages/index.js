import React, { Component } from 'react'
import styles from './index.scss'

import NextSeo from 'next-seo'

class Home extends Component {

    render() {
        return (
            <>
                <NextSeo
                    config={{
                        title: 'Nextjs Boilerplate'
                    }}
                />
                <div className={styles.home}>
                    <h3>Nextjs Boilerplate</h3>
                    <ul>
                        <li>Sass Moduling</li>
                        <li>CSS Autoprefixing</li>
                        <li>Image Loader</li>
                        <li>GraphQL Support</li>
                        <li>Console Progress Bar</li>
                    </ul>
                </div>
            </>
        )
    }
}

export default Home