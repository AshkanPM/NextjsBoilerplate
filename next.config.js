const withPlugins = require('next-compose-plugins');
const nextSass = require('@zeit/next-sass');
const nextImages = require('next-images');
const nextGraphql = require('next-plugin-graphql');
const nextProgressBar = require('next-progressbar');
const nextSize = require('next-size');

module.exports = withPlugins([
    [nextSass, {
        cssModules: true,
        cssLoaderOptions: {
            importLoaders: 1,
            localIdentName: "[local]__[hash:base64:4]",
        }
    }],
    [nextImages, {}],
    [nextGraphql, {}],
    [nextProgressBar, {
        profile: true
    }],
    [nextSize, {}]
]);